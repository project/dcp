<?php

namespace Drupal\dcp\Commands;

use Drupal\Core\Url;
use Drush\Exec\ExecTrait;
use Drush\Commands\DrushCommands;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * A drush command file.
 *
 * @package Drupal\dcp\Commands
 */
class ConfigCommand extends DrushCommands {

  use ExecTrait;

  /**
   * The module manager service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * ConfigSplitCommands constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    ModuleExtensionList $extension_list_module
  ) {
    $this->moduleHandler = $module_handler;
    $this->moduleExtensionList = $extension_list_module;
  }

  /**
   * Drush command that displays the given text.
   *
   * @param string $module_name
   *   Argument with message to be displayed.
   * @command dcp:configuration-page
   * @aliases dcp
   * @usage dcp:configuration-page devel
   */
  public function message($module_name) {
    // Check if module exists.
    if (!$this->moduleHandler->moduleExists($module_name)) {
      $this->logger->error("Module not found.");
      return;
    }

    // Check if module has a valid configuration page.
    $modules = $this->moduleExtensionList->reset()->getList();
    if (!isset($modules[$module_name]->info['configure'])) {
      $this->logger->error("Configuration page not available.");
      return;
    }

    $route = Url::fromRoute($modules[$module_name]->info['configure'], [], ['absolute' => TRUE]);
    $link = $route->toString();

    $this->startBrowser($link);
    return $link;
  }

}
